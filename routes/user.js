const express = require('express')
const router = express.Router()

const { saveUser, getUsersList, getUserDetailById, isUserExist, removeUser } = require('../services/user.service')
// const { isAuthorized } = require('../middlewares/auth.middleware')

router.get('/', async (req, res, next) => {
  try {
    const result = await getUsersList()
    res.json(result)
  } catch (error) {
    res.status(400).send(`Users List loading error:\n${error}`)
  }
})

router.get('/:id', async (req, res, next) => {
  try {
    const userId = req.params.id
    const result = await getUserDetailById(userId)
    res.json(result)
  } catch (error) {
    res.status(400).send(`User loading error:\n${error}`)
  }
})

router.post('/', async (req, res, next) => {
  const result = await saveUser(req.body)

  if (result.status) {
    res.send(result.response)
  } else {
    res.status(400).send(result.error)
  }
})

router.put('/:id', async (req, res, next) => {
  const userId = req.params.id
  const exist = isUserExist(userId)

  if (exist) {
    const result = await saveUser(req.body, userId)
    if (result.status) {
      res.send(result.response)
    } else {
      res.status(400).send(result.error)
    }
  } else {
    res.status(404).send('User does not Exist')
  }
})

router.delete('/:id', async (req, res, next) => {
  const userId = req.params.id
  const exist = isUserExist(userId)
  if (exist) {
    result = await removeUser(userId)
  } else {
    res.status(404).send('User does not Exist')
  }
})

module.exports = router
