const path = require('path')
const fs = require('fs-extra')
const { USER_LIST_DIR, USER_DETAILS_DIR } = require('../helpers/constants')

const saveUserData = async (user, id) => {
  if (user) {
    const usersListFilePath = path.format({
      dir: USER_LIST_DIR,
      base: 'fighters.json'
    })

    const users = await fs.readJson(usersListFilePath)
    const _id = id || users.length + 1

    const newUserDetailsFilePath = path.format({
      dir: USER_DETAILS_DIR,
      name: _id,
      ext: '.json'
    })

    const newUser = { _id, name: user.name, source: user.source }
    const newUserDetails = {
      _id: _id.toString,
      name: user.name,
      source: user.source,
      health: user.health,
      attack: user.attack,
      defence: user.defence
    }
    users[_id] = newUser;

    await fs.writeJson(usersListFilePath, users)
    await fs.writeJson(newUserDetailsFilePath, newUserDetails)

    return { status: true }
  } else {
    return { status: false }
  }
}

const removeUserData = async userId => {
  const usersListFilePath = path.format({
    dir: USER_LIST_DIR,
    base: 'fighters.json'
  })
  const users = await fs.readJson(usersListFilePath)
  const userIndex = users.findIndex(({_id}) => _id == userId)
  users.splice(userIndex,1)

  const userDetailsFilePath = path.format({
    dir: USER_DETAILS_DIR,
    name: userId,
    ext: '.json'
  })
  return Promise.all([
    await fs.remove(userDetailsFilePath),
    await fs.writeJson(usersListFilePath, users)
  ])
}

module.exports = {
  saveUserData,
  removeUserData
}
