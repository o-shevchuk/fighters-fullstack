const fs = require('fs-extra')
const path = require('path')
const { userValidation } = require('../helpers/user.validation')
const { saveUserData, removeUserData } = require('../repositories/user.repository')
const { USER_LIST_DIR, USER_DETAILS_DIR } = require('../helpers/constants')

const getName = user => {
  if (user) {
    return user.name
  } else {
    return null
  }
}

const saveUser = async (user, id) => {
  const result = userValidation(user)

  if (!result.length) {
    return await saveUserData(user, id)
  } else {
    return { status: null, error: 'invalid Fields: ' + JSON.stringify(result) }
  }
}

const removeUser = async(id) => {
  return await removeUserData(id)
}

const getUsersList = async () => {
  const userListFilePath = path.format({
    dir: USER_LIST_DIR,
    base: 'fighters.json'
  })
  return await fs.readJSON(userListFilePath)
}

const getUserDetailById = async id => {
  const userDetailFilePath = path.format({
    dir: USER_DETAILS_DIR,
    name: id,
    ext: '.json'
  })
  return await fs.readJSON(userDetailFilePath)
}

const isUserExist = id => {
  const userDetailFilePath = path.format({
    dir: USER_DETAILS_DIR,
    name: id,
    ext: '.json'
  })
  return fs.existsSync(userDetailFilePath)
}

module.exports = {
  getName,
  saveUser,
  removeUser,
  getUsersList,
  isUserExist,
  getUserDetailById
}
