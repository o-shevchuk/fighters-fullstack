const USER_LIST_DIR = './public/resources/api/'
const USER_DETAILS_DIR = './public/resources/api/details/fighter'

module.exports = {
  USER_LIST_DIR,
  USER_DETAILS_DIR
}