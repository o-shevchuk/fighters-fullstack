
const userValidation = user =>{
  const userObligatoryFields = [
    { key: 'name', type: 'string' },
    { key: 'health', type: 'number' },
    { key: 'attack', type: 'number' },
    { key: 'defense', type: 'number' },
    { key: 'source', type: 'string' }
  ]

 const result = userObligatoryFields.filter(({ key, type }) => typeof user[key] !== type )
 return result
}

module.exports = {
  userValidation
}