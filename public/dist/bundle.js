/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./src/javascript/app */ "./src/javascript/app.js");
/* harmony import */ var _src_styles_styles_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./src/styles/styles.scss */ "./src/styles/styles.scss");
/* harmony import */ var _src_styles_styles_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_src_styles_styles_scss__WEBPACK_IMPORTED_MODULE_1__);


window.app = new _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__["default"]();

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/lib/loader.js!./src/styles/styles.scss":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/lib/loader.js!./src/styles/styles.scss ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Imports
var urlEscape = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/url-escape.js */ "./node_modules/css-loader/dist/runtime/url-escape.js");
var ___CSS_LOADER_URL___0___ = urlEscape(__webpack_require__(/*! ../../resources/fighters-bg.jpg */ "./resources/fighters-bg.jpg"));
var ___CSS_LOADER_URL___1___ = urlEscape(__webpack_require__(/*! ../../resources/maxresdefault.jpg */ "./resources/maxresdefault.jpg"));
var ___CSS_LOADER_URL___2___ = urlEscape(__webpack_require__(/*! ../../resources/popup-bg.jpg */ "./resources/popup-bg.jpg"));

// Module
exports.push([module.i, "html,\nbody {\n  height: 100%;\n  width: 100%;\n  margin: 0;\n  padding: 0;\n  font-family: monospace; }\n\n#root {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  width: 100%;\n  user-select: none;\n  position: relative;\n  overflow-x: hidden;\n  background-image: url(" + ___CSS_LOADER_URL___0___ + ");\n  background-size: cover;\n  background-repeat: no-repeat; }\n\n#loading-overlay {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-size: 18px;\n  background: rgba(255, 255, 255, 0.7);\n  visibility: hidden; }\n\n.buttons-container {\n  min-width: 40px;\n  display: flex;\n  flex-direction: column; }\n  .buttons-container .button {\n    outline: none;\n    padding: 10px;\n    font-size: 2em;\n    font-family: monospace;\n    background-color: lightgray;\n    text-shadow: -1px -1px black, 1px 1px white;\n    color: gray;\n    border-radius: 7px;\n    box-shadow: 0 .2em gray;\n    cursor: pointer; }\n  .buttons-container .button[value=\"Hit\"] {\n    margin: auto; }\n  .buttons-container .button[value=\"Complete\"] {\n    margin-top: auto; }\n\n.text-message {\n  position: relative;\n  padding: 20px;\n  font-size: 2em;\n  text-align: center;\n  min-height: 2em;\n  text-shadow: 1px 1px 3px rgba(255, 255, 255, 0.5); }\n\n.health-bar {\n  height: 2.5em;\n  min-width: 350px;\n  margin-bottom: 2em;\n  background-color: red;\n  border: 4px solid white;\n  border-radius: 5px; }\n\n.scale {\n  height: inherit;\n  background-color: yellow; }\n\n.select-container {\n  width: 100%;\n  max-height: 45vh;\n  display: flex;\n  flex: 1;\n  flex-direction: row;\n  justify-content: space-between;\n  padding: 25px 0;\n  background-image: url(" + ___CSS_LOADER_URL___1___ + ");\n  background-size: contain; }\n\n.selected-fighter {\n  position: relative;\n  min-width: 171px;\n  margin: 0 100px;\n  flex-grow: 1.1; }\n\n.selected-fighter:nth-child(1) {\n  order: 3; }\n\n.selected-fighter:nth-child(1) {\n  transform: rotateY(-180deg); }\n\n.fighter-placeholder {\n  height: calc(45vh - 50px); }\n\n.fight {\n  position: absolute;\n  width: 100vw;\n  height: 100vh;\n  display: flex;\n  justify-content: center; }\n\n.fight .fighters {\n  pointer-events: none; }\n\n.fighters {\n  position: relative;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  flex: 1;\n  box-sizing: border-box;\n  padding: 0 15px; }\n\n.fighter {\n  display: flex;\n  flex-direction: column;\n  padding: 20px; }\n\n.fighter:hover {\n  box-shadow: 0 0 50px 10px rgba(0, 0, 0, 0.5);\n  cursor: pointer; }\n\n.name {\n  align-self: center;\n  font-size: 2em;\n  margin-top: 20px;\n  color: white; }\n\n.fighter-image {\n  max-height: 240px; }\n\n.details-container {\n  position: absolute;\n  width: 100%;\n  height: 100%; }\n\n.details {\n  max-width: 250px;\n  padding: 20px;\n  display: flex;\n  flex-direction: column;\n  background-color: #fff;\n  background-image: url(" + ___CSS_LOADER_URL___2___ + ");\n  list-style: none;\n  box-shadow: 1px 1px 5px gray;\n  animation: fadeIn 100ms ease-in;\n  border: solid 3px blue;\n  color: white; }\n\n.fade-out .details {\n  animation: fadeOut 200ms ease-out forwards; }\n\n.row {\n  padding: 5px;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between; }\n\n.key {\n  font-weight: bold;\n  min-width: 5em;\n  font-size: 1.5em;\n  line-height: 1.7; }\n\n.input-value {\n  flex-grow: 1;\n  border: none;\n  outline: none;\n  color: wheat;\n  font-size: 1.5em;\n  background: transparent;\n  border-bottom: 2px solid orange; }\n\n.text-value {\n  font-weight: bold;\n  font-size: 2em; }\n\n@keyframes fadeIn {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n\n@keyframes fadeOut {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0; } }\n", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return '@media ' + item[2] + '{' + content + '}';
      } else {
        return content;
      }
    }).join('');
  }; // import a list of modules into the list


  list.i = function (modules, mediaQuery) {
    if (typeof modules === 'string') {
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    for (var i = 0; i < this.length; i++) {
      var id = this[i][0];

      if (id != null) {
        alreadyImportedModules[id] = true;
      }
    }

    for (i = 0; i < modules.length; i++) {
      var item = modules[i]; // skip already imported module
      // this implementation is not 100% perfect for weird media query combinations
      // when a module is imported multiple times with different media queries.
      // I hope this will never occur (Hey this way we have smaller bundles)

      if (item[0] == null || !alreadyImportedModules[item[0]]) {
        if (mediaQuery && !item[2]) {
          item[2] = mediaQuery;
        } else if (mediaQuery) {
          item[2] = '(' + item[2] + ') and (' + mediaQuery + ')';
        }

        list.push(item);
      }
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || '';
  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;
  return '/*# ' + data + ' */';
}

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/url-escape.js":
/*!************************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/url-escape.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function escape(url, needQuotes) {
  if (typeof url !== 'string') {
    return url;
  } // If url is already wrapped in quotes, remove them


  if (/^['"].*['"]$/.test(url)) {
    url = url.slice(1, -1);
  } // Should url be wrapped?
  // See https://drafts.csswg.org/css-values-3/#urls


  if (/["'() \t\n]/.test(url) || needQuotes) {
    return '"' + url.replace(/"/g, '\\"').replace(/\n/g, '\\n') + '"';
  }

  return url;
};

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./resources/fighters-bg.jpg":
/*!***********************************!*\
  !*** ./resources/fighters-bg.jpg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "e9bf86c59d91b67967f9dd42eaa12903.jpg";

/***/ }),

/***/ "./resources/maxresdefault.jpg":
/*!*************************************!*\
  !*** ./resources/maxresdefault.jpg ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "90ff3fe38b1dab5a10bf282dc360e554.jpg";

/***/ }),

/***/ "./resources/popup-bg.jpg":
/*!********************************!*\
  !*** ./resources/popup-bg.jpg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ecab348e355055fabbc29ad01ef2a8c3.jpg";

/***/ }),

/***/ "./src/javascript/app.js":
/*!*******************************!*\
  !*** ./src/javascript/app.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fightersView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fightersView */ "./src/javascript/fightersView.js");
/* harmony import */ var _fightView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fightView */ "./src/javascript/fightView.js");
/* harmony import */ var _components_messageBox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/messageBox */ "./src/javascript/components/messageBox.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/fightersService */ "./src/javascript/services/fightersService.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






class App {
  constructor() {
    this.startApp();
  }

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      const fighters = await _services_fightersService__WEBPACK_IMPORTED_MODULE_3__["fighterService"].getFighters();
      const fightView = new _fightView__WEBPACK_IMPORTED_MODULE_1__["default"]();
      this.fightersView = new _fightersView__WEBPACK_IMPORTED_MODULE_0__["default"](fighters);
      this.messageBox = new _components_messageBox__WEBPACK_IMPORTED_MODULE_2__["default"]('Drag fighters to arena and click <b>Fight</b> button. <br> Click a fighter\'s portrait to edit fighter');
      const fightViewElement = fightView.element;
      const messageBoxElement = this.messageBox.element;
      const fightersElement = this.fightersView.element;
      App.rootElement.appendChild(fightViewElement);
      App.rootElement.appendChild(messageBoxElement);
      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

}

_defineProperty(App, "rootElement", document.getElementById('root'));

_defineProperty(App, "loadingElement", document.getElementById('loading-overlay'));

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/javascript/components/button.js":
/*!*********************************************!*\
  !*** ./src/javascript/components/button.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../view */ "./src/javascript/view.js");


class Button extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(value, handler, className = '') {
    super();
    this.createButtonView(value, className);
    this.element.addEventListener('click', handler, false);
  }

  createButtonView(value, className) {
    const classList = 'button ' + className;
    const attributes = {
      type: 'button',
      value: value
    };
    this.element = this.createElement({
      tagName: 'input',
      className: classList,
      attributes
    });
  }

  hide() {
    this.element.style.display = 'none';
  }

  show() {
    this.element.style.display = '';
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Button);

/***/ }),

/***/ "./src/javascript/components/detailsRow.js":
/*!*************************************************!*\
  !*** ./src/javascript/components/detailsRow.js ***!
  \*************************************************/
/*! exports provided: DetailsRow, DETAIL_TYPE */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsRow", function() { return DetailsRow; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DETAIL_TYPE", function() { return DETAIL_TYPE; });
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../view */ "./src/javascript/view.js");

const DETAIL_TYPE = {
  EDITABLE_NUMBER: 'EDITABLE_NUMBER',
  TEXT: 'TEXT'
};

class DetailsRow extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(data, type, handleChange = () => {}) {
    super();
    this.createDetailsRow(data, type, handleChange);
  }

  createDetailsRow(data, type, handleChange) {
    const keyElement = this.createKeyElement(data.key);
    const valueElement = this.createValueElement(data, type, handleChange);
    this.element = this.createElement({
      tagName: 'li',
      className: 'row'
    });
    this.element.append(keyElement, valueElement);
  }

  createKeyElement(key) {
    const keyElement = this.createElement({
      tagName: 'span',
      className: 'key'
    });
    keyElement.innerText = key;
    return keyElement;
  }

  createValueElement({
    key,
    value
  }, type, handleChange) {
    let valueElement;

    switch (type) {
      case DETAIL_TYPE.TEXT:
        valueElement = this.createElement({
          tagName: 'span',
          className: 'text-value'
        });
        valueElement.innerText = value;
        break;

      case DETAIL_TYPE.EDITABLE_NUMBER:
        valueElement = this.createElement({
          tagName: 'input',
          className: 'input-value',
          attributes: {
            value,
            type: 'number',
            min: 1,
            max: 100
          }
        });
        valueElement.addEventListener('change', event => handleChange(event, key), false);
        break;
    }

    return valueElement;
  }

}



/***/ }),

/***/ "./src/javascript/components/healthBar.js":
/*!************************************************!*\
  !*** ./src/javascript/components/healthBar.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../view */ "./src/javascript/view.js");


class HealthBar extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor() {
    super();
    this.createHealthBar();
  }

  createHealthBar() {
    this.element = this.createElement({
      tagName: 'div',
      className: 'health-bar'
    });
    this.scaleElement = this.createElement({
      tagName: 'div',
      className: 'scale'
    });
    this.element.append(this.scaleElement);
  }

  init(initialHealth) {
    this.initialHealth = initialHealth;
    this.scaleElement.style.width = '100%';
  }

  set(currentHealth) {
    const healthValueInPercent = 100 * currentHealth / this.initialHealth;
    this.scaleElement.style.width = `${healthValueInPercent}%`;
  }

  reset() {
    this.scaleElement.style.width = '100%';
  }

}

/* harmony default export */ __webpack_exports__["default"] = (HealthBar);

/***/ }),

/***/ "./src/javascript/components/messageBox.js":
/*!*************************************************!*\
  !*** ./src/javascript/components/messageBox.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../view */ "./src/javascript/view.js");


class MessageBox extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(message) {
    super();
    this.initialMessage = message;
    this.createMessageBox(message);
  }

  createMessageBox(message) {
    this.element = this.createElement({
      tagName: 'div',
      className: 'text-message'
    });
    this.set(message);
  }

  set(message) {
    this.element.innerHTML = message;
  }

  reset() {
    this.set(this.initialMessage);
  }

}

/* harmony default export */ __webpack_exports__["default"] = (MessageBox);

/***/ }),

/***/ "./src/javascript/components/selectArea.js":
/*!*************************************************!*\
  !*** ./src/javascript/components/selectArea.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../view */ "./src/javascript/view.js");
/* harmony import */ var _fighterView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../fighterView */ "./src/javascript/fighterView.js");
/* harmony import */ var _healthBar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./healthBar */ "./src/javascript/components/healthBar.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../app */ "./src/javascript/app.js");





class SelectArea extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(areaId) {
    super();
    this.areaId = areaId;
    this.handleDrop = this.handleFighterDrop.bind(this);
    this.createSelectArea(areaId);
  }

  createSelectArea(areaId) {
    this.element = this.createElement({
      tagName: 'div',
      className: 'selected-fighter'
    });
    this.element.addEventListener('dragover', this.handleDragOver, false);
    this.element.addEventListener('drop', event => this.handleDrop(event, areaId));
    this.healthBar = new _healthBar__WEBPACK_IMPORTED_MODULE_2__["default"]();
    this.fighterImage = this.createElement({
      tagName: 'img',
      className: 'fighter-placeholder',
      attributes: {
        draggable: false
      }
    });
    this.element.appendChild(this.healthBar.element);
    this.element.appendChild(this.fighterImage);
  }

  handleDragOver(event) {
    event.preventDefault();
  }

  handleFighterDrop(event, areaId) {
    event.preventDefault();

    try {
      const data = event.dataTransfer.getData('text');
      const fighter = JSON.parse(data);
      this.fighterImage.setAttribute('src', fighter.source);
      this.currentFighterId = fighter._id;
      const eventOptions = {
        bubbles: true,
        detail: {
          areaId,
          fighter
        }
      };
      const selectFighterEvent = new CustomEvent('select-fighter', eventOptions);
      this.element.dispatchEvent(selectFighterEvent);
    } catch (err) {
      console.warn('Only fighters allowed to fight');
    }
  }

}

/* harmony default export */ __webpack_exports__["default"] = (SelectArea);

/***/ }),

/***/ "./src/javascript/fightView.js":
/*!*************************************!*\
  !*** ./src/javascript/fightView.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app */ "./src/javascript/app.js");
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view */ "./src/javascript/view.js");
/* harmony import */ var _components_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/button */ "./src/javascript/components/button.js");
/* harmony import */ var _components_selectArea__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/selectArea */ "./src/javascript/components/selectArea.js");
/* harmony import */ var _helpers_fight__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./helpers/fight */ "./src/javascript/helpers/fight.js");
/* harmony import */ var _fighter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fighter */ "./src/javascript/fighter.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








class FightView extends _view__WEBPACK_IMPORTED_MODULE_1__["default"] {
  constructor() {
    super();

    _defineProperty(this, "selected", new Map());

    this.handleSelect = this.handleFighterSelect.bind(this);
    this.handleStartButtonClick = this.handleStart.bind(this);
    this.handleHitButtonClick = this.startFightRound.bind(this);
    this.handleCompleteButtonClick = this.resetFight.bind(this);
    this.createFightView();
    this.element.addEventListener('select-fighter', this.handleSelect);
  }

  createFightView() {
    this.element = this.createElement({
      tagName: 'div',
      className: 'select-container'
    });
    this.fightersArea = Array.apply(null, Array(2)).map((item, areaId) => {
      const selectArea = new _components_selectArea__WEBPACK_IMPORTED_MODULE_3__["default"](areaId);
      return selectArea;
    });
    const buttonContainer = this.createElement({
      tagName: 'div',
      className: 'buttons-container'
    });
    this.startFightButton = new _components_button__WEBPACK_IMPORTED_MODULE_2__["default"]('Fight', this.handleStartButtonClick);
    this.hitButton = new _components_button__WEBPACK_IMPORTED_MODULE_2__["default"]('Hit', this.handleHitButtonClick);
    this.completeButton = new _components_button__WEBPACK_IMPORTED_MODULE_2__["default"]('Complete', this.handleCompleteButtonClick);
    this.hitButton.hide();
    this.completeButton.hide();
    buttonContainer.append(this.startFightButton.element, this.hitButton.element, this.completeButton.element);
    this.fightersArea.map(area => this.element.append(area.element));
    this.element.append(buttonContainer);
  }

  handleFighterSelect({
    detail
  }) {
    const areaId = detail.areaId;
    const fighter = detail.fighter;
    this.selected.set(areaId, fighter);
  }

  handleStart() {
    if (this.selected.size == 2) {
      _app__WEBPACK_IMPORTED_MODULE_0__["default"].rootElement.classList.add('fight');
      this.startFight(this.selected);
      this.startFightButton.hide();
      this.hitButton.show();
    }
  }

  startFight(selected) {
    const fightersDetails = window.app.fightersView.fightersDetailsMap;
    let selectedFighters = [];
    selected.forEach((fighter, areaId) => {
      const fighterDetails = fightersDetails.get(fighter._id);
      const fighterInstance = new _fighter__WEBPACK_IMPORTED_MODULE_5__["default"]({ ...fighterDetails,
        areaId
      });
      selectedFighters.push(fighterInstance);
      const currentFighterArea = this.getFighterAreaBy(areaId);
      currentFighterArea.healthBar.init(fighterDetails.health);
    });
    this.fightIterator = Object(_helpers_fight__WEBPACK_IMPORTED_MODULE_4__["fight"])(...selectedFighters);
  }

  startFightRound() {
    const roundResult = this.fightIterator.next().value;

    if (!roundResult.winner) {
      roundResult.map(hero => {
        const {
          _id,
          health,
          areaId
        } = hero.fighter;
        const fighterHealthBar = this.getFighterAreaBy(areaId).healthBar;
        console.log(health, hero.fighter.name);
        fighterHealthBar.set(health);
      });
    } else {
      const message = roundResult.winner != 'none' ? `${roundResult.winner.name} Won` : 'They all are Dead!';
      window.app.messageBox.set(message);
      this.hitButton.hide();
      this.completeButton.show();
    }
  }

  getFighterAreaBy(id) {
    return this.fightersArea.find(area => area.areaId == id);
  }

  resetFight() {
    this.fightersArea.map(area => area.healthBar.reset());
    this.completeButton.hide();
    this.startFightButton.show();
    _app__WEBPACK_IMPORTED_MODULE_0__["default"].rootElement.classList.remove('fight');
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FightView);

/***/ }),

/***/ "./src/javascript/fighter.js":
/*!***********************************!*\
  !*** ./src/javascript/fighter.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class Fighter {
  constructor({
    name,
    health,
    attack,
    defense,
    _id,
    areaId
  }) {
    this._id = _id;
    this.name = name;
    this.areaId = areaId;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
  }

  getHitPower() {
    return this.attack * this.criticalHitChance;
  }

  getBlockPower() {
    return this.defense * this.dodgeChance;
  }

  get criticalHitChance() {
    return parseFloat(Math.random().toFixed(2)) + 1;
  }

  get dodgeChance() {
    return parseFloat(Math.random().toFixed(2)) + 1;
  }

  get isDead() {
    return this.health <= 0;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Fighter);

/***/ }),

/***/ "./src/javascript/fighterDetailsView.js":
/*!**********************************************!*\
  !*** ./src/javascript/fighterDetailsView.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app */ "./src/javascript/app.js");
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view */ "./src/javascript/view.js");
/* harmony import */ var _components_detailsRow__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/detailsRow */ "./src/javascript/components/detailsRow.js");
/* harmony import */ var _helpers_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helpers/utils */ "./src/javascript/helpers/utils.js");




const MAX_WIDTH = 300;
const MAX_HEIGHT = 250;

class FighterDetailsView extends _view__WEBPACK_IMPORTED_MODULE_1__["default"] {
  constructor(details, event) {
    super();
    this.details = { ...details
    };
    this.handleChange = this.handleDetailChange.bind(this);
    this.createDetailsView(event);
  }

  createDetailsView(event) {
    const {
      attack,
      defense,
      health,
      name
    } = this.details;
    this.element = this.createElement({
      tagName: 'div',
      className: 'details-container'
    });
    this.element.addEventListener('click', event => this.hideOnOutsideClick(event));
    const detailsListPosition = this.calculatePosition(event);
    const detailsList = this.createElement({
      tagName: 'ul',
      className: 'details',
      attributes: {
        style: detailsListPosition
      }
    });
    this.element.append(detailsList);
    const nameRow = new _components_detailsRow__WEBPACK_IMPORTED_MODULE_2__["DetailsRow"]({
      key: 'Name',
      value: name
    }, _components_detailsRow__WEBPACK_IMPORTED_MODULE_2__["DETAIL_TYPE"].TEXT);
    const attackRow = new _components_detailsRow__WEBPACK_IMPORTED_MODULE_2__["DetailsRow"]({
      key: 'Attack',
      value: attack
    }, _components_detailsRow__WEBPACK_IMPORTED_MODULE_2__["DETAIL_TYPE"].EDITABLE_NUMBER, this.handleChange);
    const defRow = new _components_detailsRow__WEBPACK_IMPORTED_MODULE_2__["DetailsRow"]({
      key: 'Defense',
      value: defense
    }, _components_detailsRow__WEBPACK_IMPORTED_MODULE_2__["DETAIL_TYPE"].EDITABLE_NUMBER, this.handleChange);
    const healthRow = new _components_detailsRow__WEBPACK_IMPORTED_MODULE_2__["DetailsRow"]({
      key: 'Health',
      value: health
    }, _components_detailsRow__WEBPACK_IMPORTED_MODULE_2__["DETAIL_TYPE"].EDITABLE_NUMBER, this.handleChange);
    detailsList.append(nameRow.element, attackRow.element, defRow.element, healthRow.element);
  }

  handleDetailChange(event, key) {
    this.details[key.toLowerCase()] = event.target.value;
  }

  hideOnOutsideClick(event) {
    if (event.target == this.element) {
      this.element.classList.add('fade-out');
      const animationTime = 200;
      setTimeout(() => {
        this.element.parentNode.removeChild(this.element);
        const closeEvent = new CustomEvent('update-details', {
          detail: { ...this.details
          }
        });
        _app__WEBPACK_IMPORTED_MODULE_0__["default"].rootElement.dispatchEvent(closeEvent);
      }, animationTime);
    }
  }

  calculatePosition({
    pageX,
    pageY
  }) {
    const normalizedX = Object(_helpers_utils__WEBPACK_IMPORTED_MODULE_3__["restrictValueByWindow"])(pageX, window.innerWidth, MAX_WIDTH);
    const normalizedY = Object(_helpers_utils__WEBPACK_IMPORTED_MODULE_3__["restrictValueByWindow"])(pageY, window.innerHeight, MAX_HEIGHT);
    return `transform:translate(${normalizedX}px,${normalizedY}px)`;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FighterDetailsView);

/***/ }),

/***/ "./src/javascript/fighterView.js":
/*!***************************************!*\
  !*** ./src/javascript/fighterView.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/view.js");


class FighterView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighter, handlers = {}) {
    super();
    this.createFighter(fighter, handlers);
  }

  createFighter(fighter, handlers) {
    const {
      name,
      source
    } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighter'
    });
    this.element.append(imageElement, nameElement);
    Object.keys(handlers).map(key => {
      this.element.addEventListener(key, event => handlers[key](event, fighter));
    });
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: 'span',
      className: 'name'
    });
    nameElement.innerText = name;
    return nameElement;
  }

  createImage(source) {
    const attributes = {
      src: source,
      draggable: 'true'
    };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });
    return imgElement;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FighterView);

/***/ }),

/***/ "./src/javascript/fightersView.js":
/*!****************************************!*\
  !*** ./src/javascript/fightersView.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/view.js");
/* harmony import */ var _fighterView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fighterView */ "./src/javascript/fighterView.js");
/* harmony import */ var _fighterDetailsView__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fighterDetailsView */ "./src/javascript/fighterDetailsView.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/fightersService */ "./src/javascript/services/fightersService.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app */ "./src/javascript/app.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







class FightersView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighters) {
    super();

    _defineProperty(this, "fightersDetailsMap", new Map());

    this.handleClick = this.handleFighterClick.bind(this);
    this.handleUpdate = this.updateFighterDetails.bind(this);
    this.handleDrag = this.handleFighterDrag.bind(this);
    this.handleSelectFighter = this.loadSelectedFighter.bind(this);
    this.createFighters(fighters);
    _app__WEBPACK_IMPORTED_MODULE_4__["default"].rootElement.addEventListener('update-details', this.handleUpdate, false);
    _app__WEBPACK_IMPORTED_MODULE_4__["default"].rootElement.addEventListener('select-fighter', this.handleSelectFighter);
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new _fighterView__WEBPACK_IMPORTED_MODULE_1__["default"](fighter, {
        click: this.handleClick,
        dragstart: this.handleDrag
      });
      return fighterView.element;
    });
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighters'
    });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    let fighterDetails = this.fightersDetailsMap.get(fighter._id);

    if (typeof fighterDetails === 'undefined') {
      fighterDetails = await _services_fightersService__WEBPACK_IMPORTED_MODULE_3__["fighterService"].getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }

    const detailsView = new _fighterDetailsView__WEBPACK_IMPORTED_MODULE_2__["default"](fighterDetails, event);
    _app__WEBPACK_IMPORTED_MODULE_4__["default"].rootElement.append(detailsView.element);
  }

  updateFighterDetails(event) {
    const fighter = event.detail;
    this.fightersDetailsMap.set(fighter._id, fighter);
  }

  handleFighterDrag(event, fighter) {
    const fighterData = JSON.stringify(fighter);
    event.dataTransfer.setData('text/plain', fighterData);
  }

  async loadSelectedFighter({
    detail: {
      fighter
    }
  }) {
    let fighterDetails = this.fightersDetailsMap.get(fighter._id);

    if (typeof fighterDetails === 'undefined') {
      fighterDetails = await _services_fightersService__WEBPACK_IMPORTED_MODULE_3__["fighterService"].getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FightersView);

/***/ }),

/***/ "./src/javascript/helpers/apiHelper.js":
/*!*********************************************!*\
  !*** ./src/javascript/helpers/apiHelper.js ***!
  \*********************************************/
/*! exports provided: callApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callApi", function() { return callApi; });
const API_URL = '/';

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method
  };
  return fetch(url, options).then(response => response.ok ? response.json() : Promise.reject(Error('Failed to load'))).catch(error => {
    throw error;
  });
}



/***/ }),

/***/ "./src/javascript/helpers/fight.js":
/*!*****************************************!*\
  !*** ./src/javascript/helpers/fight.js ***!
  \*****************************************/
/*! exports provided: fight */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fight", function() { return fight; });
function fight(fighter_1, fighter_2) {
  const fighters = [{
    fighter: fighter_1,
    opponent: fighter_2
  }, {
    fighter: fighter_2,
    opponent: fighter_1
  }];
  let fightIterator = {}; // TO DO: LEaRN HOW TO THIS USING generator & refactor

  fightIterator[Symbol.iterator] = function () {
    return {
      next() {
        fighters.forEach(attack);
        const someIsDead = fighters.some(isDead);

        if (someIsDead) {
          const result = fighters.find(hero => !isDead(hero));
          const winner = result ? result : {
            fighter: 'none'
          };
          return {
            done: true,
            value: {
              winner: winner.fighter
            }
          };
        } else {
          return {
            done: false,
            value: fighters
          };
        }
      }

    };
  };

  return fightIterator[Symbol.iterator]();
}

function attack(hero) {
  const damage = hero.opponent.getHitPower() - hero.fighter.getBlockPower();

  if (damage > 0) {
    hero.fighter.health -= damage;
  }
}

function isDead(hero) {
  return hero.fighter.isDead;
}



/***/ }),

/***/ "./src/javascript/helpers/utils.js":
/*!*****************************************!*\
  !*** ./src/javascript/helpers/utils.js ***!
  \*****************************************/
/*! exports provided: restrictValueByWindow */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "restrictValueByWindow", function() { return restrictValueByWindow; });
function restrictValueByWindow(value, windowValue, maxValue) {
  return windowValue - value > maxValue ? value : windowValue - maxValue;
}



/***/ }),

/***/ "./src/javascript/services/fightersService.js":
/*!****************************************************!*\
  !*** ./src/javascript/services/fightersService.js ***!
  \****************************************************/
/*! exports provided: fighterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fighterService", function() { return fighterService; });
/* harmony import */ var _helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/apiHelper */ "./src/javascript/helpers/apiHelper.js");


class FighterService {
  async getFighters() {
    try {
      const endpoint = 'user';
      const apiResult = await Object(_helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__["callApi"])(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const fighterEndpoint = `user/${_id}`;
      const apiResult = await Object(_helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__["callApi"])(fighterEndpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

}

const fighterService = new FighterService();

/***/ }),

/***/ "./src/javascript/view.js":
/*!********************************!*\
  !*** ./src/javascript/view.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class View {
  constructor() {
    _defineProperty(this, "element", void 0);
  }

  createElement({
    tagName,
    className = '',
    attributes = {}
  }) {
    const element = document.createElement(tagName);
    element.classList.add(className.trim());
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
    return element;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (View);

/***/ }),

/***/ "./src/styles/styles.scss":
/*!********************************!*\
  !*** ./src/styles/styles.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!../../node_modules/sass-loader/lib/loader.js!./styles.scss */ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/lib/loader.js!./src/styles/styles.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map