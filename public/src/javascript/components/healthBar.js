import View from '../view'

class HealthBar extends View {
  constructor () {
    super()
    this.createHealthBar()
  }
  createHealthBar () {
    this.element = this.createElement({ tagName: 'div', className: 'health-bar' })
    this.scaleElement = this.createElement({ tagName: 'div', className: 'scale' })
    this.element.append(this.scaleElement)
  }

  init (initialHealth) {
    this.initialHealth = initialHealth
    this.scaleElement.style.width = '100%'
  }

  set (currentHealth) {
    const healthValueInPercent = 100 * currentHealth / this.initialHealth
    this.scaleElement.style.width = `${healthValueInPercent}%`
  }

  reset(){
    this.scaleElement.style.width = '100%'
  }

}

export default HealthBar
