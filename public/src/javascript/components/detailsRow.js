import View from './../view'

const DETAIL_TYPE = {
  EDITABLE_NUMBER: 'EDITABLE_NUMBER',
  TEXT: 'TEXT'
}

class DetailsRow extends View {
  constructor (data, type, handleChange = () => {}) {
    super()
    this.createDetailsRow(data, type, handleChange)
  }

  createDetailsRow (data, type, handleChange) {
    const keyElement = this.createKeyElement(data.key)
    const valueElement = this.createValueElement(data, type, handleChange)

    this.element = this.createElement({ tagName: 'li', className: 'row' })
    this.element.append(keyElement, valueElement)
  }

  createKeyElement (key) {
    const keyElement = this.createElement({ tagName: 'span', className: 'key' })

    keyElement.innerText = key
    return keyElement
  }

  createValueElement ({ key, value }, type, handleChange) {
    let valueElement

    switch (type) {
      case DETAIL_TYPE.TEXT:
        valueElement = this.createElement({
          tagName: 'span',
          className: 'text-value'
        })
        valueElement.innerText = value
        break
      case DETAIL_TYPE.EDITABLE_NUMBER:
        valueElement = this.createElement({
          tagName: 'input',
          className: 'input-value',
          attributes: { value, type: 'number' , min:1, max:100}
        })
        valueElement.addEventListener('change', event => handleChange(event, key), false)
        break
    }
    return valueElement
  }
}

export { DetailsRow, DETAIL_TYPE }
