import App from './app'
import View from './view'
import { DetailsRow, DETAIL_TYPE } from './components/detailsRow'
import { restrictValueByWindow } from './helpers/utils'

const MAX_WIDTH = 300
const MAX_HEIGHT = 250

class FighterDetailsView extends View {
  constructor (details, event) {
    super()

    this.details = { ...details }
    this.handleChange = this.handleDetailChange.bind(this)
    this.createDetailsView(event)
  }

  createDetailsView (event) {
    const { attack, defense, health, name } = this.details
    this.element = this.createElement({ tagName: 'div', className: 'details-container' })
    this.element.addEventListener('click', event => this.hideOnOutsideClick(event))

    const detailsListPosition = this.calculatePosition(event)
    const detailsList = this.createElement({
      tagName: 'ul',
      className: 'details',
      attributes: { style: detailsListPosition }
    })

    this.element.append(detailsList)

    const nameRow = new DetailsRow({ key: 'Name', value: name }, DETAIL_TYPE.TEXT)
    const attackRow = new DetailsRow({ key: 'Attack', value: attack }, DETAIL_TYPE.EDITABLE_NUMBER, this.handleChange)
    const defRow = new DetailsRow({ key: 'Defense', value: defense }, DETAIL_TYPE.EDITABLE_NUMBER, this.handleChange)
    const healthRow = new DetailsRow({ key: 'Health', value: health }, DETAIL_TYPE.EDITABLE_NUMBER, this.handleChange)

    detailsList.append(nameRow.element, attackRow.element, defRow.element, healthRow.element)
  }

  handleDetailChange (event, key) {
    this.details[key.toLowerCase()] = event.target.value
  }

  hideOnOutsideClick (event) {
    if (event.target == this.element) {
      this.element.classList.add('fade-out')
      const animationTime = 200
      
      setTimeout(() => {
        this.element.parentNode.removeChild(this.element)
        const closeEvent = new CustomEvent('update-details', { detail: { ...this.details } })
        App.rootElement.dispatchEvent(closeEvent)
      }, animationTime)
    }
  }

  calculatePosition ({ pageX, pageY }) {
    const normalizedX = restrictValueByWindow(pageX, window.innerWidth, MAX_WIDTH)
    const normalizedY = restrictValueByWindow(pageY, window.innerHeight, MAX_HEIGHT)
    return `transform:translate(${normalizedX}px,${normalizedY}px)`
  }
}

export default FighterDetailsView
