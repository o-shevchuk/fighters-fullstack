function fight (fighter_1, fighter_2) {
  const fighters = [
    {
      fighter: fighter_1,
      opponent: fighter_2
    },
    {
      fighter: fighter_2,
      opponent: fighter_1
    }
  ]

  let fightIterator = {}
  // TO DO: LEaRN HOW TO THIS USING generator & refactor
  fightIterator[Symbol.iterator] = function () {
    return {
      next () {
        fighters.forEach(attack)
        const someIsDead = fighters.some(isDead)

        if (someIsDead) {
          const result = fighters.find(hero => !isDead(hero))
          const winner = result ? result : { fighter: 'none' }
          return {
            done: true,
            value: { winner: winner.fighter }
          }
        } else {
          return {
            done: false,
            value: fighters
          }
        }
      }
    }
  }

  return fightIterator[Symbol.iterator]()
}

function attack (hero) {
  const damage = hero.opponent.getHitPower() - hero.fighter.getBlockPower()
  if (damage > 0) {
    hero.fighter.health -= damage
  }
}

function isDead (hero) {
  return hero.fighter.isDead
}

export { fight }
