const isAuthorized = (req, res, next) => {
  if (
    req &&
    req.headers &&
    req.headers.authorization &&
    req.headers.authorization === 'admin'
  ) {
    next();
  } else {
    res.status(401).send('OPS! Not enough permissions')
  }
};

module.exports = {
  isAuthorized
}