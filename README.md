# HomeTask of ["Express yourself with Node.js" lecture](https://binary-studio-academy.github.io/stage-2/lectures/express-yourself-with-nodejs/)

Live demo https://street-fighters-node.herokuapp.com/

## How to install dependencies

Run the following commands 
- `npm i`
- `cd public/`
- `npm i`


## Development process

Open the root folder of a project and run in terminal

- `npm start`

Open another terminal instance

- `cd public/`
- `npm run build:dev`


## Deploy process

Commit to master branch triggers pipeline for deploying to heroku

Refer to repository pipelines tab to see the deploy process:
https://bitbucket.org/o-shevchuk/fighters-fullstack/addon/pipelines/home#!/

## How to Play

![fight](https://media.giphy.com/media/dySD3rZ09wk4QlG7Vm/giphy.gif)



# API descriptions

**GET**: _/user_  
get an array of all users

**GET**: _/user/:id_  
get one user by ID

**POST**: _/user_  
create user according to the data from the request body

**PUT**: _/user:id_  
update user according to the data from the request body

**DELETE**: _/user/:id_  
delete one user by ID